import React, { Component } from 'react';
import {getMovies}  from '../services/fakeMovieService.js';
import Like from './common/like.jsx';
import Pagination from './common/pagination';
import paginate from '../utils/paginate';
import ListGroup from './common/listGroup';
import {getGenres} from '../services/fakeGenreService';

class Movies extends Component {
    state = { 
        movies : [],
        currentPage :1,
        pageSize : 4,
        genres : []
     };

    componentDidMount(genere){
        this.setState({movies : getMovies() , genres : getGenres()});
    }

    handleGenreSelect = (genre) => {
        this.setState({selectedGenre : genre})
    }

     handleChangePage=(page) =>{
        this.setState({currentPage : page})
    }
    
    handleLike = (movie) =>{
         const movies = [...this.state.movies]
         const index = movies.indexOf(movie)
         movies[index]  = {...movies[index]};
         movies[index].liked = !movies[index].liked;
         this.setState({movies});
     }

     handleDelete = (movie) =>{
        
        const movies =  this.state.movies.filter(m => m._id !== movie._id)
        this.setState({movies})
     }  
    render() {
        const {length : count} = this.state.movies;
        const {currentPage,pageSize,movies : allMovies,selectedGenre} = this.state;

        if(count === 0) return <p>There are no movies in the table</p> 

        const filtered = selectedGenre ? allMovies.filter(m => m.genre._id === selectedGenre._id) : allMovies; 
        
        const movies = paginate(filtered,currentPage,pageSize)

            return (
                    <div className = "row">
                      <div className =  " col-2">
                    <ListGroup 
                    items={this.state.genres} 
                    onItemSelect = {this.handleGenreSelect}>
                    selectedItem = {this.state.selectedGenre}      
                    </ListGroup>
                    </div>
                        <div className =  "col">
                        
                            <p>There are {filtered.length} movies in the table</p>
                                <table className = 'table'>
                                    <thead>
                                        <tr> 
                                            <th>Title</th>
                                            <th>Genre</th>
                                            <th>Stock</th>
                                            <th>Rate</th>
                                            <th></th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {movies.map(movie =>
                                        <tr key ={movie._id}>
                                            <td>{movie.title}</td>
                                            <td>{movie.genre.name}</td>
                                            <td>{movie.numberInStock}</td>
                                            <td>{movie.dailyRentalRate}</td>
                                            <td>
                                                <Like liked={movie.liked}  onClick ={() => this.handleLike(movie)}/>
                                            </td>
                                            <td><button key = 'movies' onClick ={() => this.handleDelete(movie)} className="btn btn-danger btn-sm">Delete</button></td>
                                        </tr>
                                        )}
                                        </tbody>
                                </table> 
                <Pagination itemsCount={filtered.length} 
                            pageSize ={pageSize} 
                            currentPage ={currentPage}
                            onPageChange ={this.handleChangePage}/>
            </div>
        </div>

        );
    }
}
export default Movies;